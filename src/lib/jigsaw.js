/**
 * Removes a CSS class from an HTML element
 * @param { HTMLElement } element - The element
 * @param { string } className - The css class to remove
 */
const removeClass = function (element, className) {
  if (element.classList) {
    element.classList.remove(className)
  } else {
    element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ')
  }
}

/**
 * Adds a CSS class from an HTML element
 * @param { HTMLElement } element - The element
 * @param { string } className - The css class to add
 */
const addClass = function (element, className) {
  if (element.classList) {
    element.classList.add(className)
  } else {
    element.className += ` ${className}`
  }
}

/**
 * Jigsaw engine
 *
 * This is a standalone engine needing no dependencies.
 *
 * To get started, create an HTML page with a div in which the game will display, then code your interface.
 * Don't forget to include the CSS or it will be awful (_jigsaw.scss)
 *
 * For more infos on customizations, read the code.
 *
 * Example:
 *
 * ```js
 * import JigsawEngine from './jigsaw.js'
 *
 * const image = new Image()
 * image.src = 'path/or/url/to/image.gif'
 *
 * const game = new JigsawEngine(image, {
 *     gameElement: document.getElementById('game-wrapper'),
 *     tilesX: 20,
 *     onUpdate: ({ wellPlaced, total, clicks }) => {
 *       console.log('total tiles', total)
 *       console.log('well placed' wellPlaced)
 *       console.log('clicks', clicks)
 *     },
 *     onWin: () => {
 *       console.log('You won')
 *       this.game.freeze()
 *     },
 *   })
 * game.start()
 * ```
 *
 * @author Manuel Tancoigne <manu@experimentslabs.com>
 * @licence MIT
 * @param { HTMLImageElement } image - Image element
 * @param { Object }           options - An object of options
 * @param { Number=10  }       options.tilesX - Amount of tiles along X axis
 * @param { HTMLElement }      options.gameElement - HTML element where the board should be created
 * @param { Function }         options.onStart - Callback when game starts
 * @param { Function }         options.onUpdate - Callback for updates
 * @param { Function }         options.onWin - Callback for win
 * @param { Object }           options.cssClasses - Custom class names
 * @param { string='jigsaw-game' } options.cssClasses.game - Game area
 * @param { string='jigsaw-line' } options.cssClasses.line - Line of cells
 * @param { string='jigsaw-cell' } options.cssClasses.cell - Cells
 */
export function Jigsaw (image, options = {}) {
  // Game and grid
  const gameAreaWrapper = options.gameElement
  const gameArea = document.createElement('div')
  const tilesX = options.tilesX || 20

  // Keep this reference to avoid floating precision issues on multiple resizes
  let originalWidth = image.width
  let originalHeight = image.height

  let tileSize = image.width / tilesX

  // Custom css classes
  const cssClasses = options.cssClasses || {
    game: 'jigsaw-game',
    line: 'jigsaw-line',
    cell: 'jigsaw-cell',
  }

  const callbacks = {
    start: options['onStart'] || function () {},
    update: options['onUpdate'] || function () {},
    win: options['onWin'] || function () {},
  }

  // List of tiles by ID
  const tiles = {}
  // Grid of lines of cells
  const cells = []
  // Amount of well placed elements
  let wellPlaced = 0
  // Amount of swaps
  let clicks = 0
  // Currently selected cell
  let selectedCell = null
  // When frozen, tiles can't be moved
  let frozen = false

  /**
   * Creates the game
   */
  this.start = function () {
    addClass(gameArea, cssClasses.game)
    gameAreaWrapper.append(gameArea)
    build()
    shuffleTiles()

    displayCells()
    renderCells()
    updateWellPlaced()

    window.addEventListener('resize', renderCells)

    callbacks.start()
  }

  /**
   * Sorts the tiles to display image.
   */
  this.resolve = function () {
    const ids = Object.keys(tiles)
    for (let line of cells) {
      for (let cell of line) {
        cell.tileID = ids.shift()
        cell.render()
      }
    }

    updateWellPlaced()
  }

  this.freeze = function (status = true) {
    frozen = status
  }

  this.destroy = function () {
    gameArea.innerText = ''
    window.removeEventListener('resize', renderCells)
  }

  /**
   * Returns the current tiles configuration
   */
  this.currentPlacement = function () {
    const grid = []

    for (let row of cells) {
      let line = []
      for (let cell of row) {
        line.push(cell.tileID)
      }

      grid.push(line)
    }

    return grid
  }

  this.loadPlacement = function (grid) {
    for (let row in grid) {
      for (let cell in grid[row]) {
        cells[row][cell].tileID = grid[row][cell]
      }
    }

    renderCells()
    updateWellPlaced()
  }

  /**
   * Update image and tile sizes based on game area width.
   *
   * Declared early in this file as needed early...
   */
  const updateImageSizes = function () {
    const screenRatio = gameAreaWrapper.offsetWidth / originalWidth
    image.width = originalWidth * screenRatio
    image.height = originalHeight * screenRatio
    tileSize = image.width / tilesX
  }

  /**
   * Creates an ID from coordinates. Use this method to avoid hardcoding IDs
   * @param x - X position
   * @param y - Y position
   * @returns {string}
   */
  const makeID = function (x, y) {
    return `x${x}-y${y}`
  }

  /**
   * Creates an HTML cell
   *
   * @param x        - Position in grid
   * @param y        - Position in grid
   * @returns {HTMLDivElement}
   */
  const makeCell = function (x, y) {
    const cell = document.createElement('div')

    cell.className = cssClasses.cell
    cell.id = `cell-${makeID(x, y)}`

    return cell
  }

  /**
   * Creates an HTML tile
   *
   * @param x        - Position in grid
   * @param y        - Position in grid
   * @returns {HTMLImageElement}
   */
  const makeTile = function (x, y) {
    const tile = document.createElement('img')

    tile.src = image.src
    tile.id = `tile-${makeID(x, y)}`

    return tile
  }

  /**
   * Re-computes amount of good tiles and makes the game area to emit
   * an "update" event
   */
  const updateWellPlaced = function () {
    wellPlaced = 0
    const total = Object.keys(tiles).length

    forEachCell((cell) => { if (cell.good) wellPlaced++ })

    callbacks.update({ wellPlaced, total: Object.keys(tiles).length, clicks })

    if (wellPlaced === total) { callbacks.win() }
  }

  /**
   * Attaches the events to a cell
   *
   * @param cell - The cell object
   */
  const attachCellEvents = function (cell) {
    cell.html.addEventListener('click', function () {
      if (frozen) return
      if (!selectedCell) {
        selectedCell = cell
        addClass(cell.html, 'selected')
      } else {
        if (selectedCell.id !== cell.id) {
          switchTiles(selectedCell, cell)
        }

        removeClass(selectedCell.html, 'selected')
        selectedCell = null
      }
    })

    // Prevent dragging tiles
    cell.html.addEventListener('dragstart', function (event) { event.preventDefault() })
  }

  /**
   * Switches two tiles in cells
   * @param cell1 - First cell
   * @param cell2 - Second cell
   */
  const switchTiles = function (cell1, cell2) {
    const mem = cell1.tileID
    cell1.tileID = cell2.tileID
    cell2.tileID = mem

    cell1.render()
    cell2.render()

    clicks++

    updateWellPlaced()
  }

  /**
   * Creates the cells and tiles
   */
  const build = function () {
    const tilesY = Math.floor(image.height / tileSize)

    for (let y = 0; y < tilesY; y++) {
      let line = []

      for (let x = 0; x < tilesX; x++) {
        let id = makeID(x, y)
        let cell = {
          id,
          x,
          y,
          tileID: null, // Tile in this cell
          html: makeCell(x, y),
          good: false,
          render () {
            // Tile position and size
            const tile = tiles[id]
            const offsetX = x * tileSize
            const offsetY = y * tileSize
            tile.style.width = `${image.width}px`
            tile.style.height = `${image.height}px`
            tile.style.transform = `translateX(-${offsetX}px) translateY(-${offsetY}px)`

            // Display tile in cell
            this.html.append(tiles[this.tileID])

            // Cell state
            this.good = this.tileID === this.id
            addClass(this.html, this.good ? 'good' : 'bad')
            removeClass(this.html, !this.good ? 'good' : 'bad')
          },
        }
        tiles[id] = makeTile(x, y)
        attachCellEvents(cell)

        line.push(cell)
      }

      cells.push(line)
    }
  }

  /**
   * Randomly assigns tiles in cells
   */
  const shuffleTiles = function () {
    const ids = Object.keys(tiles)

    forEachCell((cell) => {
      const index = Math.floor(Math.random() * ids.length)
      cell.tileID = ids[index]
      ids.splice(index, 1)
    })
  }

  /**
   * Displays cells in game area
   */
  const displayCells = function () {
    for (let line of cells) {
      let element = document.createElement('div')
      element.className = cssClasses.line

      for (let cell of line) {
        element.append(cell.html)
        cell.render()
      }

      gameArea.append(element)
    }
  }

  /**
   * Renders cells on board (not tiles)
   */
  const renderCells = function () {
    updateImageSizes()
    gameAreaWrapper.style.setProperty('--cell-size', `${tileSize}px`)
    forEachCell((cell) => { cell.render() })
  }

  /**
   * Applies a callback to each cells
   *
   * @param { Function } callback - function to handle a cell
   */
  const forEachCell = function (callback) {
    for (let line of cells) {
      for (let cell of line) {
        callback(cell)
      }
    }
  }
}
